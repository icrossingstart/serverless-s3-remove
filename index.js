'use strict';

const glob = require('glob-all');

const globOpts = {
  nodir: true
};


class Assets {
  constructor (serverless, options) {
    this.serverless = serverless;
    this.options = options;
    this.provider = this.serverless.getProvider('aws');

    this.commands = {
      s3remove: {
        lifecycleEvents: [
          'remove'
        ],
        options: {
          verbose: {
            usage: "Increase verbosity",
            shortcut: 'v'
          }
        }
      },
      remove:{
        lifecycleEvents:[
          'remove'
        ],
        options: {
          verbose: {
            usage: "Increase verbosity",
            shortcut: 'v'
          }
        }
      }
    };

    this.hooks = {
      's3remove:remove': () => Promise.resolve().then(this.removeS3.bind(this))
    };
  }

  removeS3() {
    const service = this.serverless.service;
    const config = service.custom.assets;

    // glob
    return new Promise((resolve, reject) => {
      config.files.forEach((opt) => {
        let cfg = Object.assign({}, globOpts, {cwd: opt.source});
        var self = this;
        var $resolve = resolve;
        var $reject = reject;

        console.log("Deleting all files from : "+config.bucket);

        this.provider.request('S3','listObjects',{
          Bucket: config.bucket
        },
        this.options.stage,
        this.options.region).then(function(data){
          var items = data.Contents;
          for (var i = 0 ; i < items.length; i+=1){
            var deleteParams = {Bucket: config.bucket,Key: items[i].Key};
            console.log("- " + items[i].Key);
            self.provider.request(
              'S3',
              'deleteObject',
              deleteParams,
              self.options.stage,
              self.options.region
            );
          }
        },function(err){
          console.log("ERROR:"+err);
        });

        /*
        function (err, data) {
            if (err) {
                console.log("error listing bucket objects "+err);
                return;
            }
            var items = data.Contents;
            for (var i = 0; i < items.length; i += 1) {
                var deleteParams = {Bucket: bucket, Key: items[i].Key};
                self.deleteObject(client, deleteParams);
            }
        });
        */
        /*
        glob.sync(opt.globs, cfg).forEach((fn) => {

          const body = fs.readFileSync(opt.source + fn)
          const type = mime.lookup(fn);

          (!!this.options.verbose) && this.serverless.cli.log("File: ", fn, type)

          this.provider.request('S3', 'putObject', {
            ACL: config.acl || 'public-read',
            Body: body,
            Bucket: config.bucket,
            Key: fn,
            ContentType: type
          }, this.options.stage, this.options.region);

        });
        */
      });
      resolve();
    });
  }
}

module.exports = Assets;
